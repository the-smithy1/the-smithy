#**Objectives and Key Results**

#**February 2024**
##**Collaboration:  Define Guiding Principles**
*Owner:  Will Watkins*
<div align="left">
<b>KR1:</b>  Manifest a Social Contract ratified by 80% of the Guiding Coalition by February 29, 2024

<div class="slider-container">
    <input type="range" min="0" max="100" value="1" class="slider" id="Collaboration1">
    <div class="tooltip">0</div>
</div>

<b>KR2:</b>  Agree on Code of Conduct and Open Source License by 95% of maintainers

<div class="slider-container">
    <input type="range" min="0" max="100" value="1" class="slider" id="Collaboration2">
    <div class="tooltip">0</div>
</div>

<script src="slider-script.js"></script></div>

<!--**KR3:**-->

##**Contribution:  Create a Guiding Coalition of the Willing**
*Owner:  Will Watkins*
<div align="left">
<b>KR1:</b>  Thirty active contributors (Trello / GitLab) by February 29, 2024

<div class="slider-container">
    <input type="range" min="0" max="100" value="1" class="slider" id="Contribution1">
    <div class="tooltip">0</div>
</div>

<b>KR2:</b>  TBD

<div class="slider-container">
    <input type="range" min="0" max="100" value="1" class="slider" id="Contribution2">
    <div class="tooltip">0</div>
</div>

<script src="slider-script.js"></script></div>

##**Communication:  Attract a Volunteer Army**
*Owner:  Will Watkins*
<div align="left">
<b>KR1:</b>  Deliver 2x newsletters by February 29, 2024

<div class="slider-container">
    <input type="range" min="0" max="100" value="1" class="slider" id="Communication1">
    <div class="tooltip">0</div>
</div>

<b>KR2:</b>  Post 10x times on social media

<div class="slider-container">
    <input type="range" min="0" max="100" value="1" class="slider" id="Communication2">
    <div class="tooltip">0</div>
</div>
<!--**KR3:**-->
<script src="slider-script.js"></script></div>

##**Community: Establish a Sense of Urgency**
*Owner:  Will Watkins*
<div align="left">
<b>KR1:</b>  Expand reach to first 100 followers (LinkedIn+Instagram+Twitter+Reddit) by February 29, 2024

<div class="slider-container">
    <input type="range" min="0" max="100" value="1" class="slider" id="Community1">
    <div class="tooltip">0</div>
</div>

<b>KR2:</b>  TBD

<div class="slider-container">
    <input type="range" min="0" max="100" value="1" class="slider" id="Community2">
    <div class="tooltip">0</div>
</div>

<!--**KR3:**-->

<script src="slider-script.js"></script></div>
