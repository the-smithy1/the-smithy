#Novice
##Introduction to Trello
+ **Website**: [Trello](https://trello.com/b/CtmieIfe)
+ **Join the Smithy Workspace**:  [Invite to Board](https://trello.com/invite/b/w6QP0VGM/ATTIb959c586a3156724755b429502f3e4505E5CACF7/innersource)
+ **Start Learning**: [Getting started with Trello](https://trello.com/guide)
+ **Login Information**:
    + **Username**:  Register with your email
    + **Password**:   you'll create it when you sign in
+ **Description**: We use this as a Kanban board to allow novice contributors to collaborate on content and to track issues.

##Introduction to Slack
+ **Website**: [Slack](https://thesmithyworkspace.slack.com)
+ **Join the Smithy Workspace**:  [Invite to Slack](https://join.slack.com/t/thesmithyworkspace/shared_invite/zt-23nwnpaio-eSQ~mn~Yl~xqsKv1rEnNZw)
+ **Start Learning**: [Getting Started](https://slack.com/help/articles/360059928654-How-to-use-Slack--your-quick-start-guide)
+ **Login Information**:
    + **Username**:  Register with your email
    + **Password**:   you'll create it when you sign in
+ **Description**: Slack is our primary communication tool for collaboration.  Once you've joined, please subscribe to <code>**#innersource_collab**</code>.

##Introduction to Markdown
+ **Website**: [Dillinger](https://dillinger.io/)
+ **Start Learning**:  [The Markdown Guide](https://www.markdownguide.org/)
+ **Login Information**:
    + **Username**:  none required
    + **Password**:   none required
+ **Description**: Markdown is a simple and easy-to-use markup language you can use to format virtually any document.
