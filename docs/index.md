<link href='https://cdn.knightlab.com/libs/soundcite/latest/css/player.css' rel='stylesheet' type='text/css'><script type='text/javascript' src='https://cdn.knightlab.com/libs/soundcite/latest/js/soundcite.min.js'></script>

#**It is the Anvil, not the Hammer, that shapes steel!**

<iframe width="560" height="315" src="https://www.youtube.com/embed/9ZL-1UPjD3Q?si=FF2jCiBL7WVMOu_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

**Above, Taryn the Blacksmith and Spyder the Fae, our resident catalysts, welcome you to this intrapreneurial experience and pattern!!**

+ **Does your organisation struggle with collaboration across globally federated teams?**
+ **Have you ever attempted to create a document with five people? ten? a hundred?**
+ **Are you an "intrapreneur", disruptor, or tired of the status quo and dreaming of a better way?**

*Look no further, you've arrived at your first waypoint on the journey to better collaboration!*

*Every aspect of this experience is customisable and open!  Make a recommendation to improve the welcome video [here](https://qzvxxyn6f0c.typeform.com/to/DviY6rkq).*

##Contribution Creates a Community of Catalysts
You heard Spyder introduce the Four C's, but they are worth repeating:

+ Contribution  
+ Communication
+ Collaboration
+ Community

Additionally, we believe in Open by Default and embrace the characteristics of Transparency, Inclusivity, and Adaptability made popular by [Red Hat](https://www.youtube.com/watch?v=1aAzwzqqjkI) in open source software development.

##Join Our Adventure
Every community develops it's own traditions, and we are no different.  We want you meaningfully contributing within five minutes of joining.  Take a minute to click through the history of our community.

<iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1NzEpWomUe4j2ufGiXsenzlvhGbNSjEAGVP-s9Q8Eavk&font=Default&lang=en&initial_zoom=2&height=650' width='100%' height='650' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='0'></iframe>

*Add an event to our timeline through this [form](https://qzvxxyn6f0c.typeform.com/to/DfnxbmTv)*

Head on over to the Getting Started section to learn how to make your first contribution!!

<span class="soundcite" data-url="./assets/sound/hammer-hitting-an-anvil-25390.mp3" data-start="0" data-end="10000" data-plays="1">Remember it is the anvil, not the hammer, that shapes steel!</span>

##Maintainers
Dean Clark

Will Watkins

###Contributors

For full documentation visit [mkdocs.org](https://www.mkdocs.org).
