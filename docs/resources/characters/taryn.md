#Taryn the Blacksmith

<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53509422925/in/dateposted-public/" title="seainstorm_without_text_as_a_female_blacksmith_with_muscular_bi_4f280b99-5c6d-424f-9731-5c4d71e2af98"><img src="https://live.staticflickr.com/65535/53509422925_744d931ad9_h.jpg" width="1456" height="816" alt="seainstorm_without_text_as_a_female_blacksmith_with_muscular_bi_4f280b99-5c6d-424f-9731-5c4d71e2af98"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
**prompt**:  https://s.mj.run/iaYBmFGzJdA without text as a female blacksmith with muscular biceps, average bust, red hair, 8k --ar 16:9

**defining features**:  red hair, blacksmith, muscular arms, green eyes, modest bust

**seed**: 2871511975

**voice**: Lily, multilingual v2

**Flickr**: https://flic.kr/s/aHBqjBd64V
