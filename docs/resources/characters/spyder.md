#Spyder the Brownie
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53519767077/in/dateposted-public/" title="Spyder"><img src="https://live.staticflickr.com/65535/53519767077_dd8d1bfd5a_h.jpg" width="1456" height="816" alt="Spyder"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

**prompt**:  https://s.mj.run/vvPGFzA-Z90 small male brownie with a broom sweeping, in the art style of Howl’s Moving Castle, inside a smithy with an anvil and foundry, dressed as a blacksmith, cinematic, 8k, photorealistic --v 5.2 --ar 16:9

**defining features**:  dark hair, blacksmith, big ears, large nose, blue/green eyes, squat stature

**seed**: 1241792149

**voice**: Matthew, multilingual v2

**Flickr**: https://flic.kr/p/2pxmN9M
