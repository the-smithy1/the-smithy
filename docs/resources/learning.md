# Learning Overview
<iframe src="https://share.fireflies.ai/embed/meetings/E5GvH0ncokpJOLh4" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

In the Training/Educational Presentation meeting held on **September 9, 2024**, Ricardo Dacosta provided an insightful introduction to the **Red Hat System Administration One (RH 124)** course, focusing on essential Linux skills including command usage, file management, and system administration concepts such as managing users and groups, SSH, and file systems. Attendees were guided on accessing an online lab environment featuring various virtual machines, with instructions for setup and management. The significance of learning Linux was emphasized, highlighting its prevalence in critical technologies and industries. The discussion also covered the benefits of open-source software compared to closed-source alternatives, explaining concepts like copyleft licenses. Additionally, Dacosta differentiated between various Linux distributions and Red Hat offerings, including Fedora, CentOS, and Red Hat Enterprise Linux (RHEL), and outlined the subscription-based model for accessing Red Hat’s resources, which includes cloud features and development options.

## Notes

### 🎓 Introduction to RH 124 Course (00:34 - 01:59)
- Ricardo Dacosta introduces the Red Hat System Administration One course (RH 124).
- Course covers Linux basics: commands, file management, Vim editor.
- Emphasizes use of manpages for documentation.
- Introduces **systemd**, **NetworkManager**, **Yum**, and **DNF**.
- Covers user/group management, SSH, logging, and file systems.
- Includes comprehensive review for certification exam preparation.

### 💻 Online Lab Environment Overview (02:08 - 05:25)
- Instructions for creating and accessing the lab environment.
- Explanation of virtual machines: workstation, server a, server b.
- **Auto-stop timer** and **auto-destroy** settings explained.
- Guidance on changing screen resolution.
- Instructions for resetting virtual machines and stopping the lab environment.

### 🐧 Importance of Learning Linux (05:25 - 07:04)
- Linux is ubiquitous: planes, TVs, networking equipment, data centers.
- Powers Fortune 500 companies, stock markets, and cloud services.
- **Containers** are based on Linux.
- Open-source nature allows for faster bug fixes and increased security.

### 🔓 Open Source Software Benefits (07:04 - 09:28)
- Explanation of open source vs. closed source software.
- Benefits of open source: faster bug fixes, increased security.
- Description of **copyleft** and **permissive licenses**.
- **GPL (GNU Public License)** as an example of a copyleft license.

### 🔧 Linux Distributions and Red Hat Offerings (09:28 - 11:10)
- Differentiation between Linux kernel and Linux distributions.
- Red Hat's Linux distributions: **Fedora**, **CentOS**, **Red Hat Enterprise Linux (RHEL)**.
  - Fedora: cutting-edge, community-supported.
  - CentOS: community-supported, tracks ahead of RHEL.
  - RHEL: fully supported by Red Hat.

### 🔑 Red Hat Subscription Model (11:10 - 13:41)
- Explanation of Red Hat's subscription-based model.
- **Content Delivery Network (CDN)** for updates and new features.
- Subscription not locked to a specific RHEL version.
- **Developer subscription** option for learning and development purposes.
- Introduction to **Red Hat CoreOS** for containerization.
