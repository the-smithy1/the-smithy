#Call to Adventure

##Definition

Certainly! In "Resonate," Nancy Duarte discusses the concept of the "Call to Adventure" as a critical element in the structure of a presentation. This concept is borrowed from classic storytelling and narrative theory, particularly Joseph Campbell's "Hero's Journey," and is adapted to the context of creating engaging presentations.

The "Call to Adventure" serves as a pivotal point in a presentation. It occurs after the presenter has established the status quo ("What Is") and introduced the idea of an alternative or improved reality ("What Could Be"). Here's how Duarte describes its role and function:

1. **Challenge the Audience**: The "Call to Adventure" challenges the audience to step out of their comfort zone. It's an invitation for them to engage with a problem or opportunity that requires a response.

2. **Create Engagement**: This stage is designed to engage the audience's interest and curiosity. It marks a transition from simply informing to actively involving the audience in the narrative.

3. **Present a Choice**: At this point, the audience is presented with a choice. They can either choose to accept the call and engage with the proposed idea or solution, or they can choose to ignore it. This decision is crucial as it determines their level of engagement with the rest of the presentation.

4. **Signal a Journey**: The "Call to Adventure" signifies the beginning of a journey. It suggests that the audience is about to embark on a transformative experience, shifting from passive listeners to active participants.

Incorporating a "Call to Adventure" in a presentation is about creating a moment of decision for the audience, prompting them to actively consider and respond to the message being presented. It's a strategic move to make the presentation more interactive, impactful, and memorable.

##Prompt ChatGPT

Writing a "Call to Adventure" based on Resonate requires a deep audience-centric understanding.  To prompt for an effective Call to Adventure: you'd want to give context about:

1. **Context of the Presentation**: Briefly describe the topic or subject of your presentation. Understanding the main theme or purpose is crucial for framing the call effectively.

2. **Audience Profile**: Share information about your audience. Who are they? What are their interests, needs, or concerns related to your topic? This helps in tailoring the call to resonate with them.

3. **Current Situation (What Is)**: Explain the status quo or the current situation that your audience is familiar with. This sets the stage for introducing the contrast.

4. **Desired Change (What Could Be)**: Describe the change, improvement, or action you want to advocate for. This is what you're challenging the audience to consider or embrace.

5. **Your Goal**: Clarify what you want the audience to feel, think, or do as a result of your presentation. This helps in crafting a call that aligns with your objectives.

6. **Any Specific Constraints or Considerations**: Include any particular constraints (like time limits) or specific elements you want to incorporate or avoid in the call.

##Example for ChatGPT

<code>
Craft a Call to Adventure with the following information:

**Context of Presentation**: The presentation is about the importance of sustainable living and reducing carbon footprints. It's aimed at a general audience but with a focus on young professionals and families.

**Audience Profile**: The audience consists mostly of young professionals aged between 25-40 years, many of whom are starting families. They are generally aware of environmental issues but may not fully understand their personal impact or how to make significant changes in their daily lives.

**Current Situation (What Is)**: Begin by acknowledging the current situation – most people are living in a way that is not sustainable in the long run, often due to lack of awareness, convenience, or perceived difficulty in making changes.

**Desired Change (What Could Be)**: The desired change is for the audience to adopt more sustainable practices in their daily lives, such as recycling, using public transportation, reducing energy consumption, and supporting eco-friendly products.

**Your Goal**: The goal is to motivate the audience to start making small but impactful changes in their lifestyles. The presentation aims to shift their perspective, showing that individual actions do matter and can collectively make a significant difference.

**Specific Constraints or Considerations**: The "Call to Adventure" should be inspiring but also practical, offering realistic suggestions. Avoid making the audience feel guilty; instead, focus on empowerment and positive change.
</code>

Based on this prompt, the "Call to Adventure" could be crafted to resonate with the audience's sense of responsibility and aspiration for a better future, presenting sustainable living not just as a necessity but as an empowering choice for a healthier life and planet.

###Output
>*"Today, we stand at a crossroads. Each one of us, in our daily lives, makes choices that shape our future. We've seen how the current path we're on is unsustainable – it's a path marked by convenience and inattention to the impact we have on our planet. But what if I told you that each of us holds the power to forge a new path? A path of sustainability, responsibility, and hope.*

>*Imagine a world where your daily choices – from the way you commute, to the products you choose, to the simple act of recycling – contribute to a healthier planet. This isn't a distant dream; it's a practical, achievable reality. It's a journey that begins with small steps: choosing public transportation over personal vehicles, embracing energy-saving habits, and supporting eco-friendly products.*

>*You, as young professionals and parents, are uniquely positioned to lead this change. Your actions today will not only shape your life but also the world your children will inherit. This is your call to adventure – an invitation to embark on a journey of impactful change.*

>*Let's take this step together, not out of guilt or fear, but from a place of empowerment and optimism. The choices we make today can build a sustainable tomorrow. The question is, are you ready to take this step and be the change-makers our planet needs?"*

This "Call to Adventure" aims to strike a balance between inspiration and practicality, encouraging the audience to see themselves as pivotal agents of change in the journey towards a sustainable future.

##Best Practice

##Practical Application
<code>
Craft a Call to Adventure with the following information:

**Context of Presentation**: The presentation is about the importance of sustainable living and reducing carbon footprints. It's aimed at a general audience but with a focus on Chief Sustainability Officers.

**Audience Profile**: The audience consists mostly of young professionals aged between 25-40 years, many of whom are starting families. They are generally aware of environmental issues but may not fully understand their personal impact or how to make significant changes in their daily lives.

**Current Situation (What Is)**: Begin by acknowledging the current situation – most business are operating in a way that is not sustainable in the long run, often due to lack of awareness, convenience, or perceived difficulty in making changes.

**Desired Change (What Could Be)**: The desired change is for the audience to advocate for more sustainable practices in their organizations, such as contributing to open source projects like OS-Climate, increasing reuse through InnerSource collaboration, reducing energy consumption, and supporting eco-friendly corporate policies.

**Your Goal**: The goal is to motivate the audience to start making small but impactful changes in their lifestyles. The presentation aims to shift their perspective, showing that individual actions do matter and can collectively make a significant difference.

**Specific Constraints or Considerations**: The "Call to Adventure" should be inspiring but also practical, offering realistic suggestions. Avoid making the audience feel guilty; instead, focus on empowerment and positive change.
</code>

###Output
>*"As Chief Sustainability Officers and emerging leaders in the corporate world, you are not just at the forefront of your organizations; you are at the vanguard of a crucial movement. A movement towards a future where business and sustainability are not just aligned, but are mutually reinforcing. We're all aware of the current scenario – businesses, including ours, often operate on models that aren't sustainable in the long run. This stems from traditional practices, a focus on immediate convenience, or perceived challenges in implementing change.*

>*But what if we redefine the narrative? Imagine a future where your organization is not just a leader in its industry, but also in its impact on the planet. A future where sustainable practices are not an afterthought but are woven into the very fabric of your business strategy. This is about more than just reducing carbon footprints; it's about creating a legacy of positive, ecological impact.*

>*Your role as champions of this change is pivotal. It's time to advocate for sustainable practices within your organizations. Embrace initiatives like contributing to OS-Climate, fostering InnerSource collaboration, reducing energy consumption, and implementing eco-friendly corporate policies. These actions, though they may seem small individually, collectively have the power to drive monumental change.*

>*This is your call to adventure. An opportunity to lead not just with business acumen, but with a vision for a sustainable future. It's a journey that goes beyond personal or organizational gain – it's about shaping a world we can all be proud to leave for the next generation. The steps we take today towards sustainability are steps towards a legacy that will outlive us all.*

>*Are you ready to embark on this journey? To be the change-makers who will steer our businesses towards a sustainable, thriving future? The path is set, the blueprint is clear – the question is, will you lead the way?"* 

This "Call to Adventure" is tailored to inspire Chief Sustainability Officers and business leaders, emphasizing their unique position to drive significant and lasting change within their organizations and beyond.
