#Day by Day Breakdown

## **Monday – Understand and Define the Problem**

### **Objectives:**

- **Set a Long-Term Goal:** Define what the team aims to achieve in the long run.
- **Map the Challenge:** Create a simple map outlining the user journey or process related to the problem.
- **Understand the Problem Space:** Gather information from experts and stakeholders to identify pain points and opportunities.
- **Define Sprint Questions:** Formulate critical questions that need answers to reach the long-term goal.
- **Choose a Target:** Decide on the most important area to focus on during the sprint.

### **Activities:**

1. **Start with the End in Mind:**
     - The team discusses and agrees on the ultimate goal they want to achieve.
     - They consider what success looks like in six months, a year, or more.

2. **Create a User Journey Map:**
     - Sketch a step-by-step flow of the user's experience or the process being examined.
     - Keep it simple and high-level to visualize the problem space.

3. **Expert Interviews:**
     - Team members or external experts share insights about the problem.
     - Capture key information, assumptions, and areas of uncertainty.

4. **How Might We (HMW) Notes:**
     - As experts speak, team members write down opportunities in the form of HMW questions.
     - For example, "How might we improve the onboarding experience for new users?"

5. **Identify Sprint Questions:**
     - Discuss and list critical questions that, if answered, would help achieve the long-term goal.
     - Prioritize these questions to focus the sprint.

6. **Decide on the Target:**
     - Choose a specific segment of the user journey or a particular problem area to address during the sprint.

---

## **Tuesday – Sketch Solutions**

### **Objectives:**

- **Generate Diverse Ideas:** Encourage individual ideation to bring a variety of solutions to the table.
- **Draw on Inspiration:** Review existing solutions for inspiration.
- **Develop Detailed Solution Sketches:** Each team member creates a concrete sketch of their best idea.

### **Activities:**

1. **Lightning Demos:**
     - Team members present quick demos of products, services, or ideas that could inspire solutions.
     - The focus is on specific features or approaches, not entire products.

2. **Four-Step Sketching Process:**

     - **Notes:**
         - Review all information gathered on Monday.
         - Jot down key takeaways and ideas.

     - **Ideas:**
         - Doodle rough concepts and possible solutions.
         - Explore different angles without judgment.

     - **Crazy 8s:**
         - Fold a sheet of paper into eight sections.
         - Spend one minute per section sketching eight rapid variations or ideas.

     - **Solution Sketch:**
         - Create a detailed, three-panel storyboard of the most promising idea.
         - Ensure it's self-explanatory with clear annotations.

---

## **Wednesday – Decide on the Best Solution**

### **Objectives:**

- **Review and Critique Solutions:** Evaluate all proposed ideas critically.
- **Select the Most Promising Solution:** Use a structured process to decide which idea to prototype.
- **Create a Storyboard:** Outline a step-by-step plan for the prototype.

### **Activities:**

1. **Art Museum:**
     - Display all solution sketches anonymously on a wall or table.
     - This allows impartial evaluation of ideas.

2. **Heat Map Voting:**
     - Team members use sticky dots to mark aspects of sketches they find interesting.
     - Patterns emerge showing which ideas resonate most.

3. **Speed Critique:**
     - Discuss each sketch briefly, focusing on highlights and concerns.
     - Capture standout ideas and potential issues.

4. **Straw Poll and Decider Vote:**
     - Team members vote on their favorite solution.
     - The Decider (usually the team lead or stakeholder) makes the final decision.

5. **Storyboard Creation:**
     - As a group, create a detailed storyboard mapping out the prototype.
     - This serves as a blueprint for Thursday's prototyping.

---

## **Thursday – Prototype**

### **Objectives:**

- **Build a Realistic Prototype:** Create a tangible version of the chosen solution.
- **Prepare for Testing:** Ensure the prototype is ready for user interactions.

### **Activities:**

1. **Assign Roles:**
     - **Maker(s):** Responsible for building specific parts of the prototype.
     - **Stitcher:** Combines different parts into a cohesive whole.
     - **Writer:** Crafts any text needed (e.g., labels, instructions).
     - **Asset Collector:** Gathers images, icons, or other assets.
    - **Interviewer:** Prepares for conducting user tests on Friday.

2. **Prototype Mindset:**
     - Focus on **"Goldilocks Quality"**—just real enough to elicit genuine responses.
     - Use tools and materials that allow for quick development (e.g., digital mockups, paper prototypes).

3. **Build the Prototype:**
     - Follow the storyboard closely.
     - Pay attention to details that affect user perception.

4. **Trial Run:**
     - Test the prototype internally to catch any issues.
     - Make necessary adjustments before user testing.

---

## **Friday – Test with Users**

### **Objectives:**

- **Validate the Prototype:** Gather feedback from real users.
- **Learn and Iterate:** Identify what works, what doesn't, and why.

### **Activities:**

1. **Conduct User Interviews:**
     - Schedule sessions with five target users (a sufficient number to spot patterns).
     - The interviewer guides users through the prototype while observing their reactions.

2. **Observation and Note-Taking:**
     - Team members watch the interviews live (often from another room) via video feed.
     - Use a structured note-taking approach to capture observations.

3. **Debrief and Synthesize Findings:**
     - After each interview, briefly discuss initial impressions.
     - At the end of the day, review all findings to identify trends and key insights.

4. **Decide on Next Steps:**
     - Determine whether to proceed with the solution, iterate on it, or consider alternative ideas.
     - Reflect on the sprint questions and whether they were answered.

---

## **Key Principles Throughout the Sprint**

- **Time-Boxed Activities:** Strict time limits keep the team focused and the sprint on schedule.
- **Collaboration and Autonomy:** While teamwork is essential, individual work prevents groupthink and encourages diverse ideas.
- **User-Centric Focus:** Decisions are guided by the needs and feedback of real users.
- **Iterative Approach:** The sprint is a cycle of building, testing, and learning to refine ideas quickly.

---

## **Conclusion**

The sprint process provides a clear, efficient path from problem to tested solution in just five days. By dedicating each day to a specific set of tasks, teams can:

- **Monday:** Align on goals and understand the problem deeply.
- **Tuesday:** Generate innovative solutions individually.
- **Wednesday:** Critically evaluate options and commit to the best one.
- **Thursday:** Build a realistic prototype without unnecessary perfectionism.
- **Friday:** Test with users to gain valuable insights and validate assumptions.
