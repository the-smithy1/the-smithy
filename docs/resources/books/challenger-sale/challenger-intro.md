# Start Here
Applying generative AI to the Challenger Sale is both an art and a science.  Below we have broken out some useful variables that help with prompt engineering; however, it is essential to constantly tweak and fix to transform into a single narrative.

##Variables
- Audience: <code>Early to mid career professionals interested in learning new skills to avoid obsolescence</code>
- Motif: <code>Medieval Blacksmith in a fantasy style</code>
- Tone:  <code>Playful and Inspirational</code>
- Key Insights: <code>Importance of collaboration, contribution, communication and community to adopting Platform Engineering</code>
- Goal of Warmer:  <code>Investing in tools alone is transformation theatre.  Unless you create a different context, you cannot change behaviour, which evolves a new mindset that catalyses a cultural shift.</code>
- Current Belief:  <code>Platform Engineering applies only to software engineers, and isn't accessible by non technical stakeholders</code>
- New Perspective/Idea:  <code>As software engineering concepts have crept further down the technical stack, the discipline can also unlock business advantage by lowering cognitive load and accelerative time to value through fae magic.</code>
- Objective of Reframe:  <code>Challenge the audience's misconception that fae magic technology is only disrupting the technical landscape.</code>
- Topic or Issue:  <code>The half-life of relative skills is only accelerating, which requires individuals to continuously learn to stay ahead of their peers</code>
- Key Data Points & Insights:
- Desired Realization:
- Desired Emotional Response:
- Real-life Consequences:
- Background information:
- Current Belief or Practice:  *Community, collaboration, and technology*
- Desired Outcome:  *Transformative potential in reshaping business processes through InnerSource*
- Unique Value Proposition:  *If we build it, they won't come.  If they build it, they will love it!*
