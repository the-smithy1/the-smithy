#Overview

**The Art of Action: How Leaders Close the Gaps between Plans, Actions, and Results**

In "The Art of Action," Stephen Bungay explores the disconnect often observed in organizations between strategic plans, their execution, and the actual results achieved. Drawing on principles from military history, particularly the tactics of the 19th-century Prussian military, Bungay provides a comprehensive guide for business leaders to bridge these gaps.

<iframe width="560" height="315" src="https://www.youtube.com/embed/f-V3LW5Bmi0?si=ixD30Qajf0Wr0RIV" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Key Concepts:

**The Three Gaps:**

   - **Knowledge Gap:** The difference between what we know and what we need to know to make decisions.
   - **Alignment Gap:** The discrepancy between what we want people to do and what they actually do.
   - **Effects Gap:** The variance between what we expect our actions to achieve and what they actually achieve.

**Mission Command:**
   - Derived from military doctrine, mission command emphasizes decentralization, where leaders set clear objectives and allow subordinates the freedom to decide how to achieve them. This approach fosters adaptability and initiative.

**Clarity, Commitment, and Communication:**
   - Bungay emphasizes the importance of clear goals, committed teams, and effective communication. Leaders must ensure that everyone understands the strategic intent, is motivated to act, and maintains open lines of communication.

**Directives and Intent:**
   - Instead of detailed instructions, leaders should focus on conveying the overall intent. This allows teams to adapt their actions in response to changing circumstances while staying aligned with the strategic objectives.

**Cycle of Operations:**
   - Bungay outlines a continuous process of planning, action, and review. Leaders should constantly assess the situation, adjust their plans, and communicate changes, fostering a dynamic and responsive organizational culture.

### Practical Applications:

- **Leading with Intent:** Leaders should provide a clear vision and objectives but trust their teams to find the best ways to achieve them.
- **Building Trust and Empowerment:** Empowering teams requires a foundation of trust and the belief that individuals are capable and motivated to take initiative.
- **Adapting Military Techniques to Business:** Bungay translates military strategies into practical business applications, showing how companies can be more agile and effective in their operations.

### Conclusion:

"The Art of Action" offers a robust framework for improving organizational performance by closing the gaps between plans, actions, and results. By adopting principles of mission command and focusing on clarity, commitment, and communication, leaders can create more adaptive, aligned, and effective organizations.

<iframe type="text/html" sandbox="allow-scripts allow-same-origin allow-popups" width="336" height="550" frameborder="0" allowfullscreen style="max-width:100%" src="https://lesen.amazon.de/kp/card?asin=B01HPVHLHG&preview=inline&linkCode=kpe&ref_=cm_sw_r_kb_dp_QX8HYRW5R2ZNM20STXB4" ></iframe>
