#**Introduction**
Author:  **Jeanne Liedtka**

Publication:  Harvard Business Review 2018

[Source](https://hbr.org/2018/09/why-design-thinking-works)

---------------------

"Why Design Thinking Works" by Jeanne Liedtka explores the effectiveness of design thinking in fostering innovation within organizations. Here are some key points:

### Summary of "Why Design Thinking Works"

**Introduction to Design Thinking**:
   - Design thinking helps innovation teams overcome biases and entrenched behaviors that typically hinder the application of innovative practices.
   - It's compared to Total Quality Management (TQM) in manufacturing for its potential to unleash creative energies and improve processes.

**Challenges of Innovation**:
   - Superior solutions, lower risks and costs, and employee buy-in are crucial for successful innovation.
   - Common obstacles include defining problems conventionally, difficulty in understanding unarticulated customer needs, and managing diverse perspectives within teams.

**Role of Design Thinking**:
   - Design thinking’s structured process helps teams navigate human biases and organizational barriers.
   - It involves tools like ethnographic research, problem reframing, experimentation, and diverse team collaboration.

**Steps in the Design Thinking Process**:

   - **Customer Discovery**: Immersion in user experiences to identify hidden needs. Examples include understanding behaviors through direct observation and engagement.
   - **Sense Making**: Organizing qualitative data into themes and patterns to extract insights.
   - **Alignment**: Workshops and discussions to align team members on design criteria and goals.
   - **Idea Generation**: Brainstorming and developing potential solutions while addressing underlying assumptions and biases.
   - **Prototyping and Testing**: Creating low-cost artifacts to capture user experiences and iterating based on feedback.

**Examples and Case Studies**:
   - Various examples are provided from sectors like health care and social services, demonstrating how design thinking reshaped innovation processes and led to significant improvements in outcomes.

### Applications of Design Thinking

- **Overcoming Biases**: By immersing themselves in users' experiences, innovators shift their mindsets, which leads to better understanding and more effective solutions.
- **Managing Qualitative Data**: Structured processes help innovators make sense of large volumes of data and derive actionable insights.
- **Building Consensus**: Design thinking fosters alignment within teams, helping them converge on what matters most to users.
- **Generating and Testing Ideas**: Encourages the emergence of new ideas and their validation through real-world experiments, reducing fear of change.

### Conclusion

Design thinking provides a structured approach to innovation that helps organizations address human biases, align teams, and generate superior solutions. By involving stakeholders throughout the process, it garners broad commitment to change and facilitates the successful implementation of innovative ideas.

If you have any specific questions or need more detailed information from the document, please let me know!
