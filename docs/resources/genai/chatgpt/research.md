
# Guidelines for Using ChatGPT as a Personal Research Assistant

## 1. State the Topic Clearly
Be specific about the topic you want to explore. The more focused the topic, the better the results.
- **Example**: "I need research on the impact of AI in the music industry, particularly how musicians are using AI tools to create music."

## 2. Define the Scope
Specify which aspects of the topic you want to focus on—such as historical context, current trends, future predictions, or specific case studies.
- **Example**: "Focus on how AI is changing the role of music producers and songwriters over the last 5 years."

## 3. Set the Format
Indicate how you want the information to be presented (summary, in-depth analysis, comparison, etc.).
- **Example**: "Please provide an in-depth analysis with examples of AI tools used in music creation, including challenges and benefits."

## 4. Indicate Any Sources or Tools
Mention if you have specific sources to include or avoid.
- **Example**: "Use academic papers and reports from reputable tech or music industry journals, but avoid purely promotional articles."

## 5. Context or Background
Provide any relevant context or goals for the research.
- **Example**: "I'm looking into integrating AI into a music learning platform targeted at 8-12-year-olds, so include educational use cases if possible."

## 6. Optional: Timeframe or Urgency
Specify if there's a deadline or urgency for your request.
- **Example**: "Please provide this research within 3 days, as I have a presentation next week."

## 7. Request Hyperlinks for Source Material
To ensure references are easily accessible, request hyperlinks when available.
- **Example**: "Please use credible academic and industry sources, and include hyperlinks to source material when available."

---

## Example Prompt
"Can you research the impact of AI on the music industry, specifically focusing on the role of producers and songwriters in the last 5 years? I'd like an in-depth analysis with examples of AI tools, challenges, and benefits, especially in relation to music learning platforms for children. Please use credible academic and industry sources, and include hyperlinks to source material when available. I need this within 3 days for a presentation."
