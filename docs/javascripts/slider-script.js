document.querySelectorAll('.slider').forEach(function(slider) {
    slider.style.background = 'white';

    slider.oninput = function() {
        var value = this.value;
        this.nextElementSibling.innerHTML = value; // Update the tooltip

        if(value < 30) {
            this.style.background = 'red';
        } else if(value >= 30 && value < 70) {
            this.style.background = '#FFBF00';
        } else {
            this.style.background = 'green';
        }
    }
});
