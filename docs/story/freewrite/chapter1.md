#Chapter One - Pull, Not Push

##Outline
- Old carpenter/cabinet maker using the forge to stay warm
- Lost the strength in his arms needed to cut wood
- Taryn returns from the market and hears him cough
- She invites him inside to sit by the fire while she prepares the food
- He is intrigued by her knives, which cut on the pull stroke
- She explains that they were a gift from the previous owner who had gifted them to her on his deathbed, along with the Smithy
- A spider in the corner begins to take notice
- The Cabinetmaker explains that he wouldn't need to sleep in her forge, if he could only work again
- Taryn takes out a piece of slate and starts drawing, asking the Cabinetmaker to clarify certain details
- After she makes him a bed near the fire, she retires for the evening
- The moment the night is fully descended, the Spyder transforms into an old weathered Brownie licking his lips at the cauldron still warm on the fireplace
- After he finishes off the stew, he begins to tinker with the piece of slate, making small notations in his spidery scrawl
- With more questions that answers, he attempts to draw a prototype of the new saw, complete with instructions
- Before he retires for the night, he adds some spices and venison to Taryn's grocery list.  Spyder wonders aloud what the place might look like with some nice cabinets.
- The Cabinetmaker wakes with the dawn and decides he can at least run to the market for groceries to be useful
- When Taryn wakes, she sees the new notes and the illustrated prototype, so she fires up the forge and starts to work

**Notes**:
- With many eyes, all bugs are shallow -> flexibility, durability
- Use quality ingredients -> ebonwood too hard
- Too old of a cat to be fucked by a kitten

---

- If I'm trying to appeal to an audience of 6-8 year olds, then I need a character they can see themselves in.
- Taryn - Worldly
- Cabinetmaker - Wise
- Spyder - Wonder

Ebonwood Ash needed in the Flux
- helps remove impurities (separation of slag) during smelting molten metal

Pull, not Push
- precision, clean cuts
- cuts across grain without tearing the wood fibers
- hardwoods need right tooth configuration and technique

Chisel for Turning on a lathe
Twylight -> Character name

---

She heard the cough, before she saw the old man.  It sounded like a death rattle echoing through her forge, the raspy assault echoing off the cold bare stone.  When she found him, perched up against the barely warm chimney, he didn’t look far from her sounded, either.

“Come on Gramps,” she mumbled as she prodded the sack of bones with her boot.  “You’ll catch your death out here, and I could use an extra pair of hands.” She nodded down at her overly full hands, having just returned from the market.

Perhaps it was the smell of freshly baked bread that finally roused the old dodger, but she caught the narrow glint of his reddish glare as he opened one eye.  As he struggled to rise, she heard enough creaks and pops of his weathered joints that she feared winter had already set deep into his bones.

“Beg your pardon, lass, I must’ve dozed off for a moment there,” he creaked, his voice wavering as he pulled himself upright.  “I’ll just be on my way, never you mind.”

“Nonsense, you old fart, I won’t you dying on my doorstep,” Taryn said gruffly, but the twinkle in her eyes and warm smile belied her harsh words.  “Help me with the door, and I’ll start a fire.”

As they shuffled inside, Taryn caught a glimpse of the old man’s bare arms through his threadbare cloak.  The skin seemed to hang over the bones of his upper arms like laundry drying on a line, the muscle all but atrophied.  It wasn’t the first time, and she doubted it would be the last, she had witnessed grisly aftermath of the wasting disease.

Taryn stoked the embers of the fire into a gentle flame, sitting the old man on a bench before it.  As she noticed the vitality begin to seep back into his face, she handed over the iron poker, so that she could put away her groceries.  After a few hacking coughs, she watched as the old timer’s head slumped on his shoulders, a deep sonorous snore escaping that gave her a steady rhythm to her mundane chore.

Slowly, the morning twilight gave way to sunrise, and golden beams of sunlight stirred motes of dust inside the smithy.

“I’ve seen more furniture in a troll’s cave,” Taryn heard the reedy voice say, only now realizing that the snoring had stopped.  “I had thought this place had been abandoned after Old Tam bundled off to the war.”  Taryn smiled wanly as she set down the basket she had been mending.  

“It’s good to see you still have some salt and vinegar, old timer,” she replied, as she sidled up next to him.  “I’ve only moved in yesterday evening, so you’ll forgive me if I haven’t yet found the time to decorate.”  

“Chisel,”he said  quietly, “My name is Chisel the Cabinetmaker.”

“Oh a cabinetmaker, eh?  I could do with a few of those,” she said, surveying the sparse surroundings. “Well met Chisel.”

“Would that I could lass, but I’ve lost my strength to push the saw and a cabinetmaker is only as good as his strong arm.”  Chisel brushed away a single tear and continued to stoke the fire absently.

“Well your tongue seems to work well enough,” she said with a grin as she handed him a crust of bread.  “Put this in your gob, so I can have some peace and quiet, while I rustle up some eggs from the henhouse.”  Taryn watched as most of the loaf disappeared into Chisel’s gullet, along with a couple of eggs and bacon she was able to muster up.
