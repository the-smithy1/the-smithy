#Early Thoughts
####Saturday, 9th of March, 2024

Taryn pulled a requisition order at random from the drawer.  At the very top, in the previous master's scrawl, as the word, "Impossible!"  From what she could tell, the local cabinet maker needed a saw to cut through hard woods.  On further investigation, though, she began to see her predecessor's problem.  The local mill produced hardwood planks of a standard length for a reasonable price, and any deviation came at a premium cost.  The cabinet maker planks cut an even number of equal segments of a standard length that need to be long enough to accommodate the smallest cabinet.  The crux of the problem came down to the kerf of the saw blade.  It needed to be thick enough to handle the pressure of compression against the hardwood, but not so thick so as to waste too much wood when sawing.  With every previous attempt, the thickness of the blade had created a kerf large enough that the plank would yield an odd number of segments of the correct length, and a final wasted segment that was just a little short of the standard.

Taryn decided the puzzle was intriguing, and sent a runner for the cabinet maker.

When the cabinet maker arrived, the former masters comment suddenly made more sense.  She had wondered what was wrong with the metallurgy formula that made it "Too Weak", but watching the cabinetmaker teeter through the yard, she doubted he could lift her lightest hammer, even when he had been on the better side of seventy.  In his wake, a young sapling of a girl hefted his saws and hardwood plank, for all the world looking like a willow wand with jagged metallic leaves and one eye poking through a mop of hair.

Without conscious thought, Taryn reached out to lighten the young girl's load, but no sooner had she extended her hand than a clammy limp calloused hand enclosed her own.

"Pleasure to make your acquaintance, Smith!"  Folks call me Young Cub."  Observing Taryn's smile deepened into a frown as she looked past him to the girl, cub tightened his grip for a moment, leaning into whisper, "Don't worry about my grand daughter.  Her Da died in the war, but she's tough as nails!"

Before Taryn could start a retort, the mop of hair obscuring {grand-daughter}'s face parted in the wind, revealing a grimace of fierce determination.  Cub looked back at her too, and a moment of intense pride washed over his visage.  Taryn ushered them both inside, and bade them sit down in front of the hearth, within earshot of Spyder's web.

It didn't take Taryn long to conclude that Cub was both a master of his craft and a shrewd businessman.  She'd expected {grand-daughter} to doze by the fire while the adults discussed business, but instead she took voracious notes in a tidy left handed script.  After about an hour, Taryn had narrowed the problem down to three parts.

"The previous master told you compression of the blade and arm strength are the two major constraints," Taryn saw Cub's eyes begin to droop, "but, you also require precision."

"I can be precise with my saws," he replied, pride bubbling back into his eyes.

"Yes you can," she said, looking sideways at {grand-daughter}, "but it requires a great deal of strength and control."  Taryn began sketching her prototype on the slate.  "What you need is a blade that is waifer thin, long enough to cut through a plank in a single pass, and narrow enough to fit into hard to reach places."

Taryn looked down at her slate in dismay.  Even the strongest metal she could craft wouldn't hold to such specifications, and even if she had it within her gift, Cub would never be able to afford it.

---

Cub's Lesson:  Precision
- Joinery
- Working w/ inadequate tools

Taryn's Lesson:  Addition by Subtraction
- Brute Force not always better
- Smarter not Harder

Spyder's Lesson:  Pull over Push
- Contribution Creates
- Resilience through ReUse

{grand-daughter}'s Lesson:  Fierce Determination (Mind over Matter)
- Bandages on fingers from sharpening saw
- A blunt tool expertly wielded . . .
