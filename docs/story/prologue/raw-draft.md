# **Prologue**

Once upon a time, in a sylvan hamlet long forgotten by travellers and passersby, two rivers converged. The Passion, aptly named for its surging rapids, called to the heartstrings like the moon tugs at the sea—full of whitewater and waterfalls. The Perseverance ground through the mountain gorge—steady and smooth—steeled with determined will. Over time, the locals, both mortal and Fae, became as intertwined as their tributaries and chose to call their community **Grit**.

Far from the nearest caravan route and without the need for coin, trust was the currency of the realm. Fear not, if you are unaccustomed to its pure sound, like an infant's laugh in silent spaces or a cat's contented purr, you will know the ring of trust when you hear it.  Contribution, collaboration, and communication created a community.  And so it is here, at a small smithy nestled near the convergence, that the peal of truth pulls us strongly. With every hammer fall of the newly arrived blacksmith, the magic beckons.

But pay close attention for it is the anvil, not the hammer, that shapes steel.

---
