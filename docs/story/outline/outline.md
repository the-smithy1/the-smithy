#Outline for The Smithy

#Story Development Template

1. **Genre and Subgenre**: Fantasy, and specifically high fantasy.

2. **Plot Overview**: I want the story to be about how good communication, collaboration, and contribution create community and triumph over individualism, bureaucracy, and power/status.

3. **Main Characters**:
     - The main protagonists are Taryn the Blacksmith and Spyder the Brownie, her apprentice.  
     - Taryn is a generalist who has had many trades and vocations.  She was a cook and barmaid in her youth, then spent her earlier adulthood in the military.  Disenchanted with the war, she hung up her sword for the hammer when her mentor died on the battlefield and left her his smithy.
     - Spyder is a magical Brownie who often take the shape of a spider, specifically a member of the forge Far, who is limited to the confines of the smithy in the mortal world.  He is descended from a long line of blacksmith’s, but sadly his apprenticeship was cut short when his father died unexpectedly.  As a specialist, he can manipulate his magic to do amazing things, but he is limited by his experience and imagination.

4. **Setting**:
     - The story takes place in the village of Grit, which lies at the convergence of two rivers, the Passion and the Perseverance (or “Old Persey” to the locals).  The village folk and their local Fae embody the attributes for which their town and rivers are named.  It starts as Taryn arrives into town to start anew as the local blacksmith.
     - communication and collaboration between the mortal and Fae realms is accomplished through music as magic.  Within the Fae realm, the magical beings are organized in families or tribes, similar to nomadic Gypsy troupes.  
     - a war continues to ravage the lands beyond Grit, but it has not yet reached the village.

5. **Themes and Messages**:
     - I want to explore the tragedy of the commons weighed against the power of community contribution.  
     - Open source software development principles will underly successful communities.  
     - Stewardship, resilience over compliance, and platform thinking will also be explored.  
     - Trust is the currency between high performing teams, and hierarchy only serves to benefit the few over the many.  
     - I like the concepts that “A jack of all trades is a master of none, but sometimes a master on none is better than a master of one,” and the idea that innovation is rarely about creating something new, but instead it is about applying something that was successful in a different context to the current problem.

6. **Tone and Style**:
     - The mood should be both sardonically witty and inspirational.  
     - I prefer to follow the precepts laid out in Stephen King’s book, “On Writing”.

7. **Point of View**:
     - The point of view should be third-person limited from the perspective of Spyder and Taryn.

8. **Key Scenes or Events**:
     - Each chapter should follow a consistent formula where a character comes to visit with a particular problem that Taryn and Spyder need to solve.  Through collaboration with the new character and his or her Fae, the new community grows and is reinforced.  Eventually other towns begin to take notice and start to mimic the village of Grit, often providing new ideas.  When the inevitable war does eventually reach the borders of the kingdom, it doesn’t require a battle, as the ideas have already crossed over into the adversary, who it turns out was never an evil enemy just different.

9. **Target Audience**:
     -  I want to target young adults from 15 to 18.

10. **Story Length**:
     -  I would like to write a series of parables that are short story length.

11. **Existing Material**:
     - Attached

12. **Inspirations and Influences**:
     - I really liked the style of Lloyd Alexander, when I was the target age.  Additionally, Patrick Rothfuss, Robin Hobb, and Scott Lynch are heavy influences of mine.
