#**Raw Draft**
##Chapter 1

A death rattle just this side of the living echoed through her forge, the raspy assault bouncing off the cold bare stone.  When she found the source, perched up against the barely warm chimney, Taryn feared she was already too late.

“Come on Gramps,” she mumbled as she prodded the sack of bones with her boot.  “You’ll catch your death out here, and I could use an extra pair of hands," she nodded down to two large sack straining at the stitching.

Perhaps it was the smell of fresh baked bread that roused the old dodger, but she caught the narrow glint of his reddish glare as he opened one eye.  She heard enough creaks and pops of his weathered joints as he pulled himself to his feet with a thin willow cane that Taryn feared winter had already set too deep in his bones.

“Beg your pardon, lass, I must’ve dozed off for a moment there,” he creaked, his voice wavering as he pulled himself upright.  “I’ll just be on my way, never you mind.”

“Nonsense, you old fart, I won’t have you dying on my doorstep when I've only just arrived,” Taryn said with a twinkle in her eyes and warm smile, “plus it takes days to get the smell out.  Help me with the door, and I’ll rouse the fire.”

As they shuffled inside, Taryn caught a glimpse of the old man’s bare arms through his threadbare cloak.  The skin seemed to hang over the bones of his upper arms like laundry drying on a line, the muscle all but atrophied.  She had hoped it would be another lifetime before she was face to face with the grisly aftermath of the wasting disease, instead of a few short months since her time on the front lines.

Taryn stoked the embers of the fire into a gentle flame, sitting the old man on a bench before it.  As she noticed the vitality begin to seep back into his face, she handed over the iron poker, so that she could put away her sundries.  

Placing his cane in the corner with careful reverent care, the man managed to reawaken the hearth to a gentle roar.  After a few hacking coughs, the old timer’s head slumped on his shoulders, a deep sonorous snore setting a rhythm to her morning chores.

Slowly, the twilight gave way to sunrise, and golden beams of light stirred motes of dust inside the smithy.  She was surprised to find it in such excellent repair.  The floors were swept clean, and the forge and anvil stood tidy and oiled. She glanced at the well-made lock she had opened with her key and decided to inquire about town so she could thank the mysterious caretaker properly. The only sign of neglect was an elaborate spiderweb filling one corner.

The web was a masterpiece.  Intricate strands wove outward from the center, overlapping and reinforcing each other. The fading sunlight gave it an ethereal glow, and she tried not to contemplate the relative size of the spider to match the web.  It's creator was nowhere in sight.

"Somebody has too much time on their hands," Taryn quipped, but who was she to destroy such beauty merely because it might frighten her.  "Just don't make me regret it," she said as she turned away and started to hum as she went about her chores.

---

He heard the call before he opened his eyes, the old Fae magic faint but pure.  More embarrassed than alarmed, Spyder was surprised to see the old cabinetmaker asleep at the fire and a strange woman humming as she unpacked her belongings.  

He listened closer for the telltale drumbeat or gentle pipe melody that summoned his kind into the mortal realm, but there were none at hand.  Instead, the woman hummed a light harmony over the cabinetmaker's snoring.  It was simple, natural, and intoxicating.

She carefully placed a collection of leather-bound books on a shelf, handling each with tender care.  She pulled out an old drum of making that had seen decades of use.  Spyder was relieved when she settled the drum into it's customary cranny, as though an old friend had returned home.

Next came a piece of slate, a blacksmith's apron, a hammer, and a pair of tongs. All appeared brand new, functional, and uninspiring.

Finally, she unwrapped a tightly packed bundle revealing well-polished knives and an array of dried spices. The spices she arranged in a tiny kitchen near the cauldron, but Spyder knew his father's masterwork even with the knife's makers mark facedown on the counter.

She might have come across the drum and key to the smithy by some odd luck, but Old Tam, the previous master blacksmith, would never have parted with his prized possession, not while he was alive anyway.

The cabinetmaker's stillness betrayed his feigned sleep, and Spyder saw his shoulders slump at the answer to the question neither wanted to ask.  On it's own, his cane jostled in place, two gnarled knots revealing a pair of sympathetic eyes trained on the old man.  They exchanged a quick glance, before he opened his mouth to speak.

---

“I’ve seen more furniture in a troll’s cave,” Taryn heard a reedy voice say, only now realizing that the snoring had stopped.  “I had thought this place had been abandoned after Old Tam was bundled off to war.”  

“It’s good to see you still have some salt and vinegar, old timer,” she replied, as she sidled up next to him.  “I’ve only moved in last night, so you’ll forgive me if I haven’t yet found the time to decorate.”  

“Chisel,” he rasped, “They call me Chisel the Cabinetmaker.”

“Oh a cabinetmaker, eh?  I could do with a few of those,” she said, surveying the sparse surroundings.

“Would that I could lass, but I’ve lost the strength to push the old saw,” he guffawed with a crude wink as Taryn smothered a chuckle.

“Well your tongue seems to work well enough, even if your head's on loose,” she said with a grin as she handed him a crust of bread.  “Put this in your gob, so I can have some peace and quiet, while I rustle up some eggs from the henhouse.”  

Taryn watched as most of the loaf disappeared into Chisel’s gullet, along with a couple of eggs and bacon she mustered up.  As he slumped over again in a true contented slumber, Taryn convinced herself that his wheeze sounded just a little better.  

All her worldly possessions snug and in place, she sat down at the counter and stared absently at the spiderweb.  The kitchen knife had been a gift from the smithy's previous owner, and as always, she marvelled at it's craftsmanship. It never seemed to lose it's edge, and the balance and heft were perfect in her hand.  

Tender memories tinged with the sharpness of loss surfaced as she remembered evenings spent with its maker, and not for the last time she sighed at the futility of war.  As she raised it up to the sunlight, a single beam reflected off the blade into the shadowy recesses of the rafters.  Eight eyes reflected back as the web's architect scuttled out of the light.    

"To things left unsaid," she mumbled as she set the knife down on the cutting board, and with a faint smile and a glance at the spider, "and  to bloody big bastards."  Through some acoustic trick, she could've sworn she heard a whimper from the shadowy corner, even though Chisel was fast asleep behind her.

Glancing sideways at her gaunt house guest, she said to herself, "Tomorrow I'll pick up some venison, but for tonight, vegetable stew will have to do."  A smile touched her lips as she reminisced about cooking with her gran at the family tavern.

She sank into the reverie of comfortable routine.  Vegetables chopped themselves, the cauldron was set to a gentle simmer, and the fire was set for a long stew as Taryn's mind wandered.  Still, there was a nagging feeling she couldn't shake that she was forgetting something important as she finished preparing the meal.

With nothing pressing to do, she glanced at the hammer atop the anvil. Retrieving a piece of chalk, she began to sketch on the slate. She had developed quite a knack for drawing during her travels, so she rendered a passable likeness of the hammer in no time.

Her thoughts drifted back to the knife, and she found herself making unconscious corrections to the hammer's design. She hefted it, noting it was a bit top-heavy for her liking, and made a note beside the sketch. The handle was too large for her grip, and the angle didn't favor the precision she required. Like so many things, it was built with only a man in mind, all brute strength and power, but no finesse.

Sketch complete, Taryn pulled a book from the shelf and settled into a chair. It wasn't long before her eyes grew heavy too, the day's journey catching up with her. She sank slowly into a lazy nap, the book resting in her lap, her stew bubbling on the cozy hearth.

---

"She's forgotten the thyme and rosemary," a voice squeaked from the cobwebbed corner.  As the spider descended from his web on a twinkling line of silver thread, the shadows shifted and a crack of the fire revealed a tiny adolescent Fae creature, licking his lips as he stared at the stewing cauldron.  "If we don't add it soon, the taste will be ruined!"

"Hush child, and help an old woman to the door," piped up from the corner, dry bark rustling as the cane unfolded into a gnarled willow forest Fae.  "Still an apprentice, eh Spyder," she patted his head as she passed him in the doorway, "I cannot imagine it's been easy being cooped up in this smithy all this time the master was away."  She delicately plucked the herbs from the garden and handed them to the forge Fae.

"Thank you Master Twylight!" Spyder mumbled as he raced back to the cauldron.  "I don't remember the last time I had vegetables, even the flies stopped coming around the last few weeks."  He shuddered as he glanced at his web.

"Times have been hard on all of us."  Twylight reached up to rub Chisel's arm as she creaked past him to join Spyder at the fire.  Spyder produced two bowls from thin air and served her first despite his obvious excitement.  The serving spoon had barely settled back into the cauldron before he was licking the bowl clean.

Hunger sated, Spyder's gaze wandered to the knives resting neatly on the counter.  The maker's mark on the blade danced with the flames.  Twylight caught his eye.  "I was sorry to hear about your father, youngling.  He will be missed."  Spyder nodded and continued on to the slate sitting in front of Taryn.

"She has the right of it," he remarked as he perused the drawing with a look of desire nearly equal to his initial hunger.  "A few minor tweaks here and here . . . " he trailed off in thought as Twylight moved to join him, the same glint of hunger reflected in her own visage.

---
