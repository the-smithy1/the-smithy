#Raw Notes from my First Notebook 2024

##Page 1
- Tonal Center
  -  Ear training and being able to recognize
  -  Gypsy music from Range book
  -  Something Else

Instead of Battle Rounds, Music Measures

Align Learning / Experience system to focused / diffused learning

Mix of ShadowRun / DragonRealms w/ Augmented Reality

Earn Experience Through Trackable Actions, but create a gate to circle that requires skill synthesis and practical application

Novice --> Grandmaster

Diamond Shaped Individuals (General, Specialized, Domain)

##Page 2
Multi User Dungeon (MUD) Integrated with Music

* Wearable Items "resonate" with Different Notes/Chords/Etc
* Metaverse Register on Physical Apple Wearables

  *  Heat
  2.  Vibration
  3.  Electric Static Shock

- Timing/Beat could create a slowly building multiplier

##Page 3
