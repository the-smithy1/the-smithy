#Onboarding Overview

- Create Slack channel [account_name]-[country]-pursuit
- Create blank GitLab repository
- Copy account template into [account_name] directory
- Integrate GitLab for Slack into
- Update mkdocs.yml to match GitLab repository
- Trigger Initial Commit by following instructions for "Existing project" from GitLab
- Add, Rename ([account_name] Strategic Pursuit Portal), and Pin GitLab pages into Slack channel
- Create a Timeline Google Spreadsheet from [Template](https://timeline.knightlab.com/?_gl=1*nhbc1i*_ga*MTk0NzAxOTY1OS4xNzE3NDA2MzAw*_ga_8F4WPDMPL5*MTcxNzQwNjI5OS4xLjAuMTcxNzQwNjI5OS4wLjAuMA..#make)
- Rename Timeline to [account_name] Official Timeline JS3
- Publish Timeline to Web
- Embed Timeline to index.md
- 
