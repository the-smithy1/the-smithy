#HTML Cheatsheet

##Style
###JavaScript
**Variables**
- names should be descriptive.
- use const as default
- **let multiNameVariable**: use camel case for variables with multiple names.
- **const UPPERCASE_LETTERS**:  use UPPERCASE letters and snake_case for constants.

**Classes**
- should always start with an UPPERCASE letter

##Glossary
**Anchor (a):**
**Attribute:**  provide extra information and they go inside the opening tag. (Ex.  <a href="">, <img src="">)
**Box Model:** Elements on a webpage are rectangular boxes with four layers: content, paddings, borders, and margins.
**Cascading Style Sheets** (CSS):  a styling language for great-looking websites.
**Classes:** templates for objects in JavaScript
**Element:**  The opening tag, closing tag, and code in between.
**Empty Tag:**  no closing tag and no content.  They are also called self-closing tags. (Ex. <br>, <img>)
**Flex Container:** the parent element we apply flexbox to
**Flex Item(s):** the HTML elements inside the flex container
**Flexbox:** a new CSS feature designed to adapt webpages to different screen sizes.
**Head Element** (<head></head>):  a container for information about a webpage, like its title.
**Hypertext Reference** (href): an attribute that links to a URL
**IDs**:  identify HTML unique elements
**Margins:** generate space around an element, outside of any paddings and borders.
**Paddings:** generate space between the content and the border of an element.
**Rule**:  formed by a selector and its properties in CSS
**Style Sheet**:  a special file just for styling the webpage.
**Synchrony**:  a term used for code instructions that execute one after another.
**Uniform Resource Locater** (URL):
**Variables:** content and names that tell us what's inside in JavaScript (let, const).
