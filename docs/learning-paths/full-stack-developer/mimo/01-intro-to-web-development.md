#**01. Intro to Web Development**

[Link](https://mimo.org/web/50/section/0)
Date Completed:  01/10/2024
Last Updated:  01/10/2024

##Bottom Line Upfront
Excellent introduction to HTML and CSS with a practical application to build a personal linktree website.  Project Extensions were great as well!

##Cycles
**Learn/Practice Pomodoro:** Lessons 01-06
**Guided Project:** Lesson 07
**Learn/Practice:** Lessons 08-16
**Guided Project:** Lessons 17
**Enhancement:** Make your website pop, Add icons to your website, Add animations to your website
