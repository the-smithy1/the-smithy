#Introduction to Weekly Newsletter

At a recent team event, we went around the table and highlighted what drives us on a daily basis in a few simple words.  When it was my turn to speak, I didn't need to think very long . . .

. . . Continuous Learning has always been my one and only superpower or competitive advantage in my industry or profession.  Whether it is reading, writing, tinkering, destroying, rebuilding, teaching, or mentoring, I always find myself falling back on the skills, growth mindset, and passion I have developed over my lifetime.

These days, the amount of content available can be overwhelming, so sit back and relax while I curate a few topics for you to peruse at your leisure.
