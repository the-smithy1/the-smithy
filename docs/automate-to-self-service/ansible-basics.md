#Ansible Basics

Plays:  Top level specification for a group of tasks
  - Building blocks for playbooks

Modules ("tools in the toolkit"):  Parametized components with internal logic, representing a single step to be done.
  - Language: usually Python or Powershell for Windows, but can be any language.

Plugins:  pieces of code that augment Ansible's core functionality.

Inventory:  list of systems in your infrastructure that automation is executed against
  - works against multiple systems in an inventory
  - usually file based
  - can have multiple groups
  - can have variables for each group or even host

Roles:  reusable structure of tasks and variables.

Collections:  a data structure containing automation content
  - Modules
  - Playbooks
  - Roles
  - Plugins
  - Docs
  - Tests

Execution Environments (EE): components needed for automation, packaged in a cloud-native

Execution Environment Builder (Builder): bundle build, create, and publish

Automation Content Navigator:  develop, test, run
